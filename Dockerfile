FROM centos:7

RUN yum install -y dnf make rsync

# Install qemu
RUN curl -s -L https://github.com/multiarch/qemu-user-static/releases/download/v4.0.0/qemu-aarch64-static -o qemu-aarch64-static && chmod +x qemu-aarch64-static
RUN mkdir -p /arm/sysroot/usr/local/bin && mv qemu-aarch64-static /arm/sysroot/usr/local/bin

COPY install.sh /arm/
RUN /arm/install.sh

RUN mkdir -p /arm/compiler
RUN cd /arm/compiler && curl -s -L https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz | tar Jxf -

ENV PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.20.0/Linux-x86_64/bin:$PATH
ENV COMPILER=/arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu
ENV SYSROOT=/arm/sysroot

COPY aarch64-toolchain.cmake /arm
