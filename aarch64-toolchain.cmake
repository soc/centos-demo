set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)
set(CMAKE_SYSROOT $ENV{SYSROOT})

set(tools $ENV{COMPILER})

set(CMAKE_C_COMPILER ${tools}/bin/aarch64-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER ${tools}/bin/aarch64-linux-gnu-g++)

set(CMAKE_CXX_LINK_FLAGS " /cvmfs/sft.cern.ch/lcg/contrib/gcc/8/aarch64-centos7/lib64/libstdc++.so " CACHE STRING "LDFLAGS" )
