#!/bin/bash

function dnf_cmd {
    dnf -y \
        --forcearch=aarch64 \
        --releasever=7 \
        --verbose \
        --installroot=/arm/sysroot \
        --nogpgcheck \
        $@
}

dnf_cmd "groupinstall 'Minimal Install'"
dnf_cmd "install glibc-devel nfs-utils"

sed -e 's|root:\*|root:|g' -i /arm/sysroot/etc/shadow
